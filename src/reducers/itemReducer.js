import Item1 from '../images/item1.jpg'

const initState = {
    items: [
        { id: 1, title: 'One Plus 8 Pro', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 110, img: Item1 },
        { id: 2, title: 'Iphone 11', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 80, img: Item1 },
        { id: 3, title: 'Samsung G11', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 120, img: Item1 },
        { id: 4, title: 'Mi 11 Pro', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 260, img: Item1 },
        { id: 5, title: 'Lenovo', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 160, img: Item1 },
        { id: 6, title: 'HTC M8 Pro', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price: 90, img: Item1 }
    ]

}
const itemReducer = (state = initState, action) => {

    //INSIDE HOME COMPONENT
    if (action.type === "ADD") {
        let num = (Math.floor((Math.random() * 6)));
        let imageNum = (num === 0) ? (num + 1) : num;
        let image = "../images/item"+imageNum+ ".jpg";
        console.log(image)
        let id = state.items.length + 1
        return {
            items: [
                {
                    id: id,
                    title: action.name,
                    desc: action.description,
                    price: action.price,
                    img: Item1
                },
                ...state.items
            ]
        }
    } else {
        return state
    }

}

export default itemReducer
