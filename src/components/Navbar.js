import React from 'react';
import { Link } from 'react-router-dom'
 const Navbar = ()=>{
    return(
            <nav className="nav-wrapper">
                <div className="container">
                    <Link to="/" className="brand-logo">Trendz</Link>
                    
                    <ul className="right">
                        <li><Link to="/">Device List</Link></li>
                        <li><Link to="/add-item">Add Item</Link></li>
                    </ul>
                </div>
            </nav>
   
        
    )
}

export default Navbar;