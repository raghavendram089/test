import React, { useState } from 'react';

export default function InputItem({ label, value, updateValue }) {
    const [inputValue, setInputValue] = useState(value)

    return (
        <div>
            <label>
                {label}
                <input type="text" name="name" value={inputValue} onChange={(e) => {
                    var numbers = /^[0-9]+$/;
                    if ((label === 'Price' || label === 'Quantity') && !e.target.value.match(numbers)) {
                        return;
                    }
                    setInputValue(e.target.value)
                    updateValue(e.target.value)
                }} />
            </label>
        </div>
    );
}