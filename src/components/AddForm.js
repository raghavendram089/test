import React, { Component } from 'react'
import InputItem from './InputItem';
import { connect } from 'react-redux'
import { addItem } from '../actions/itemActions'
import { withRouter } from 'react-router-dom';

class AddForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            price: 0,
            color: "",
            quantity: 0,
            vendor: ""
        }
    }

    updateName = (value) => {
        this.setState({
            name: value
        })
    }
    updateDescription = (value) => {
        this.setState({
            description: value
        })
    }
    updatePrice = (value) => {
        this.setState({
            price: value
        })
    }
    updateColor = (value) => {
        this.setState({
            color: value
        })
    }
    updateQuantity = (value) => {
        this.setState({
            quantity: value
        })
    }
    updateVendor = (value) => {
        this.setState({
            vendor: value
        })
    }

    handleSubmit = (event) => {
        const { name, description, color, vendor, quantity, price } = this.state
        if (!name) {
            alert("Name is Required")
            return
        }
        if (!description) {
            alert("Descrition is Required")
            return
        }
        if (!color) {
            alert("Color is Required")
            return
        }
        if (!vendor) {
            alert("Vendor is Required")
            return
        }
        if (!quantity) {
            alert("Quantity is Required")
            return
        }
        if (!price) {
            alert("Price is Required")
            return
        }
        event.preventDefault();
        this.props.dispatch(addItem(name, description, color, vendor, quantity, price))
        this.props.history.push('/');
    }
    render() {
        return (
            <div style={{ width: '100%', marginLeft: '25%' }}>
                <form style={{ width: '75%' }} onSubmit={this.handleSubmit} >
                    <InputItem label="Name" value="" updateValue={this.updateName} />
                    <InputItem label="Description" value="" updateValue={this.updateDescription} />
                    <InputItem label="Vendor" value="" updateValue={this.updateVendor} />
                    <InputItem label="Color" value="" updateValue={this.updateColor} />
                    <InputItem label="Price" value="" updateValue={this.updatePrice} />
                    <InputItem label="Quantity" value="" updateValue={this.updateQuantity} />
                    <input type="submit" value="Submit" style={{ width: '96px', height: '40px', backgroundColor: '#ee6e73', color: "#fff" }}
                    />
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}


export default connect(
    null,
    mapDispatchToProps
)( withRouter(AddForm));
