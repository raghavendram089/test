//add item action
export const addItem= (name, description, color, vendor, quantity, price)=>{
    return{
        type: "ADD",
        name, 
        description, 
        color, 
        vendor, 
        quantity, 
        price
    }
}
