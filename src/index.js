import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css'
import itemReducer from './reducers/itemReducer';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

const store = createStore(itemReducer);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));    
