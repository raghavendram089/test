import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Navbar from './components/Navbar'
import Home from './pages/Home'
import AddItem from './pages/AddItem';

class App extends Component {
  render() {
    return (
       <BrowserRouter>
            <div className="App">
            
              <Navbar/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/add-item" component={AddItem}/>
                  </Switch>
             </div>
       </BrowserRouter>
      
    );
  }
}

export default App;
