import React, { Component } from 'react';
import { connect } from 'react-redux'
import AddForm from '../components/AddForm'

class AddItem extends Component{

    render(){
              
       return(
        <div className="container">
            <h3 className="center">Add Device</h3>
                <div className="box">
                    <AddForm/>
                </div>
            </div>
       )
    }
}


const mapStateToProps = (state)=>{
    return{
        items: state.addedItems,
        //addedItems: state.addedItems
    }
}
const mapDispatchToProps = (dispatch)=>{
    return{
        
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(AddItem)